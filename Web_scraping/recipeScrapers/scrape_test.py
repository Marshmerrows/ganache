from recipe_scrapers import scrape_me

# give the url as a string, it can be url from any site listed below
scraper = scrape_me('https://www.epicurious.com/recipes/food/views/gluten-free-chocolate-tahini-brownies')

# scraper.title()
# scraper.total_time()
# scraper.yields()
ingredients = scraper.ingredients()
ins = scraper.instructions()
# scraper.links()

n = 0
for i, q in enumerate(ins):
    if q == '°':
        print(ins[i-3:i])
