units = ['cup', 'ounce', 'oz', 'tsp', 'teaspoon', 'tablespoon', 'tbsp', 'g', 'points', 'items', '°']

ingredients = ['walnut','pecan','hazelnut','chocolate','almonds','pistachio','coconut','cherr','berr','baking powder','baking soda','candy','avocado','brown sugar','flour','sugar','egg','vanilla','salt','cinnamon','oil','cream','espresso','coffee','mint','marshmallow']
standard_units = ['cup','cup','cup','cup','cup','cup','cup','cup','cup','tsp','tsp','cup','items','cup','cup','cup','items','tsp','tsp','tsp','cup','cup','cup','cup','items','cup','°','points','servings']
# item_conv = [1/7,1/24, 0.0033068733333333,0.188,'almonds','pistachio','coconut','cherr','berr','baking powder','baking soda','candy','avocado','brown sugar','flour','sugar','egg','vanilla','salt','cinnamon','oil','cream','espresso','coffee','mint','marshmallow']
