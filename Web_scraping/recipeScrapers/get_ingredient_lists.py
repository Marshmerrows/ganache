# from recipe_scrapers import scrape_me
import csv
import numpy as np
from recipe_scrapers import scrape_me
import ref

csv_write_file = 'brownie_data.csv'
csv_read_file = 'recipe_links_test.csv'

def has_numbers(input_string):
    print('has_numbers')
    if type(input_string) is not 'str':
        input_string = str(input_string)
    return any(char.isdigit() for char in input_string)

def remove_pars(string):
    print('remove_pars')
    N = len(string)

    start_par = string.find('(')

    while start_par is not -1:
        end_par = string.find(')')
        string = string[0:start_par-1]+string[end_par+1:N]
        start_par = string.find('(')

    return string

def get_links():
    print('get_links')
    with open(csv_read_file, 'r') as f:
        reader = csv.reader(f)
        space_recipe_links = list(reader)

    N = len(space_recipe_links)

    recipe_links = []

    for n in range(0,N,2):
        print('append')
        recipe_links.append(space_recipe_links[n])

    for q, link in enumerate(recipe_links):
        print('enumerate')
        link = str(link)
        link = link.replace('[','')
        link = link.replace(']','')
        link = link.replace('\'','')
        link = link.replace('\'','')

        recipe_links[q] = link

    return recipe_links

def process_data_set(csv_read_file,recipe_links):
    print('process_data_set')

    M = []
    for link in recipe_links:
        A = [[0 for _ in range(2)]  for _ in range(len(ref.ingredients) + 3)]

        scraper = scrape_me(link)
        ingredients = scraper.ingredients()
        ins = scraper.instructions()
        rating = str(scraper.ratings()) + ' points'
        yields = scraper.yields()
        # # scraper.links()
        # # scraper.title()
        # # scraper.total_time()

        #
        for i, q in enumerate(ins):
            if q == '°':
                temp = ins[i-3:i] + ' °'

        ing_list = []
        for item in ingredients:
            item = remove_pars(item)
            parsed = item.split(' ')

            if has_numbers(parsed[0]):
                if parsed[0][-1] == 'g':
                    item_desc = ' '.join(parsed[1:len(parsed)])
                    parsed[1] = parsed[0][-1]
                    parsed[0] = parsed[0][:-1]

            if len(parsed) is 1:
                item_desc = ' '.join(parsed)
                parsed[0] = 1
                parsed.append('items')

            if not has_numbers(parsed[0]):
                item_desc = ' '.join(parsed)
                parsed[0] = 1
                parsed[1] = 'items'

            if has_numbers(parsed[1]):
                parsed[0] = parsed[0]+parsed[1]
                parsed[1] = parsed[2]
                item_desc = ' '.join(parsed[3:len(parsed)])
            else:
                item_desc = ' '.join(parsed[2:len(parsed)])

            amount = parsed[0]
            unit = parsed[1]

            unit_found = 0
            for item in ref.units:
                if unit.find(item) is not -1:
                    unit_found = 1
            if unit_found == 0:
                unit = item
                amount = parsed[0]
                item_desc = ' '.join(parsed[1:len(parsed)])

            ing_found =0
            for q, item in enumerate(ref.ingredients):
                if item_desc.find(item) is not -1:
                    A[q][0] = amount
                    A[q][1] = unit

            A[-2][0] = rating.split(' ')[0]
            A[-2][1] = rating.split(' ')[1]
            A[-3][0] = temp.split(' ')[0]
            A[-3][1] = temp.split(' ')[1]
            A[-1][0] = yields.split(' ')[0]
            try:
                A[-1][1] = yields.split(' ')[1]
            except:
                A[-1][1] = 'items'

            M.append(A)

    return M

def standardize_units(M):
    print('standardize_units')
    for q,A in enumerate(M):
        # print(f'M{q}={A}')
        for i in range(0,len(A)):
            if A[i][0] == 0:
                A[i][1] = ref.standard_units[i]
                # print(A[i][1])
            if A[i][1] != ref.standard_units[i]:
                unit = A[i][1]
                unit_list = ' '.join(ref.units)
                if unit_list.find(unit) != -1:
                    conv_ratio = 1
                    esc_num = 0
                    while True:
                        if esc_num > 3:
                            conv_ratio = 1
                            break

                        if unit == 'g':
                            conv_ratio *= 1/4.2 #g to tsp
                            unit = 'tsp'
                            if ref.standard_units[i] == 'tsp':
                                break

                        if unit == 'tsp' or unit == 'teaspoon':
                            conv_ratio *= 1/3 #tsp to tbsp
                            unit = 'tbsp'
                            if ref.standard_units[i] == 'tbsp' or ref.standard_units[i] == 'tablepsoon':
                                break

                        if unit == 'tbsp' or unit == 'tablespoon':
                            conv_ratio *= 0.5 #tbs to oz
                            unit = 'oz'
                            if ref.standard_units[i] == 'oz' or ref.standard_units[i] == 'ounce':
                                break

                        if unit == 'oz' or unit == 'ounce':
                            conv_ratio *= 1/8 #oz to cup
                            unit = 'cup'
                            if ref.standard_units[i] == 'cup':
                                break

                        if unit == 'cup':
                            conv_ratio *= 128 #cups tp g
                            unit = 'g'
                            if ref.standard_units[i] == 'g':
                                break

                        esc_num += 1
                    # print(f'A[i][0] = {A[i][0]}')
                    # print(f'conv_ratio = {conv_ratio}')
                    converted = conv_ratio*eval(str(A[i][0]))
                    A[i][0] = converted
                    # print(f'A[i][0] after = {A[i][0]}')
        new_array = []
        for m in range(0,len(A)):
            new_array.append(A[m][0])

        M[q] = new_array

    return M

def normalize_from_yield(M):
    print('normalize_yield')
    for q,A in enumerate(M):
        for i in range(0,len(A)):
            try:
                A[i] = float(eval(A[i]))/float(A[-1])
            except:
                print(f'A[i]:{A[i]}')
                print(f'A[-1]:{A[-1]}')
        M[q] = A

    return M



if __name__ == '__main__':
    header = ref.ingredients
    recipe_links = get_links()
    M = process_data_set(csv_read_file,recipe_links)
    M = standardize_units(M)
    M = normalize_from_yield(M)
    print(M)

    # with open(csv_write_file, "w") as csv_file:
    #     writer = csv.writer(csv_file, delimiter=',',header=header)
    #     for q in range(0,len(M)):
    #         csv_file.writerow(M[q])

# np.savetxt("recipe_links.csv", unique_links, delimiter=",", fmt='%s')
