import numpy as np
from selenium import webdriver
from selenium.webdriver.common.keys import Keys

# recipe_websites = [
# "http://101cookbooks.com/",               ## skipped
# "http://allrecipes.com/",
# "http://bbc.com/",
# "http://bbc.co.uk/",
# "http://bbcgoodfood.com/",
# "http://bonappetit.com/",
# "http://closetcooking.com/",
# "http://cookstr.com/",
# "http://epicurious.com/",
# "http://finedininglovers.com/",
# "http://foodnetwork.com/",
# "http://foodrepublic.com/",
# "https://geniuskitchen.com/",
# "http://giallozafferano.it/",             ## skipped
# "https://healthyeating.nhlbi.nih.gov/",   ## skipped
# "https://www.hellofresh.com/",            ## skipped
# "https://www.hellofresh.co.uk/",          ## skipped
# "https://inspiralized.com/",              ## skipped
# "http://jamieoliver.com/",
# "http://mybakingaddiction.com/",
# "http://paninihappy.com/",                ## skipped
# "http://realsimple.com/",                 ## skipped
# "http://simplyrecipes.com/",
# "http://steamykitchen.com/",              ## skipped
# "https://www.tastesoflizzyt.com",
# "http://tastykitchen.com/",
# "http://thepioneerwoman.com/",
# "http://thehappyfoodie.co.uk/",
# "http://thevintagemixer.com/",            ## skipped
# "http://twopeasandtheirpod.com/",
# "http://whatsgabycooking.com/",
# "http://yummly.com/"
# ]


### All Lists
# General purpose lists
links = []
recipe_links = []
unique_recipe_links = []


#####
# Epicurious Links

for q in range(1,9):
    recipe_website = f'https://www.epicurious.com/search/brownies?content=recipe&page={q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)
    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)
for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# Allrecipes Links

for q in range(1,10):
    recipe_website = f'https://www.allrecipes.com/search/results/?wt=brownies&sort=re&page={q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)
    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)
for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# bbc Links

for q in range(1,5):
    recipe_website = f'https://www.bbc.co.uk/search?q=brownies&filter=food&suggid=#page={q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)
    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)
for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# bbcgoodfood Links

for q in range(0,3):
    recipe_website = f'https://www.bbcgoodfood.com/search/recipes?query=brownies#query=Brownies&page={q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)
    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)
for element in recipe_links:
    if element not in bbc_links:
        bbcgoodfood_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# bonappetit Links

for q in range(1,5):
    recipe_website = f'https://www.bonappetit.com/search/brownies?content=recipe&page={q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)
    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)
for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# closetcooking Links
recipe_website = 'https://www.closetcooking.com/?s=brownies'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)
for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))
for link in links:
    if link is not None:
        for item in link.split('/'):
            if item == 'recipes':
                recipe_links.append(link)
                print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# cookstr Links
for q in range(1,4):
    recipe_website = f'https://www.cookstr.com/task/search/search_term/brownies/page/{q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if item == 'recipes':
                        recipe_links.append(link)
                        print(link)
for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# finedininglovers Links

recipe_website = 'https://www.finedininglovers.com/search-box/?brownies'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)
for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# foodnetwork Links
for q in range(1,116):
    recipe_website = f'https://www.foodnetwork.com/search/brownies-/p/{q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if item == 'recipes':
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# foodrepublic Links
recipe_website = 'https://www.foodrepublic.com/?s=brownies'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)
for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# geniuskitchen Links
for q in range(1,36):
    recipe_website = f'https://www.food.com/search/brownies?pn={q}'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if item == 'recipe':
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
# links = []
# recipe_links = []

#####
# giallozafferano Links
#
# for q in range(1,36):
#     recipe_website = 'https://www.giallozafferano.it/ricerca-ricette/brownies/'
#     driver = webdriver.Firefox()
#     driver.get(recipe_website)
#     print('!!!!!!!!!!!!!!!')
#     print(q)
#
#     for a in driver.find_elements_by_xpath('.//a'):
#         links.append(a.get_attribute('href'))
#
#         for link in links:
#             if link is not None:
#                 for item in link.split('/'):
#                     if item == 'ricette.giallozafferano.it':
#                         recipe_links.append(link)
#                         print(link)
#
# for element in recipe_links:
#     if element not in unique_recipe_links:
#         unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# jamieoliver Links
recipe_website = 'https://www.jamieoliver.com/search/?s=brownies'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)
for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))
    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipe':
                    recipe_links.append(link)
                    print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# mybakingaddiction Links
for q in range(1,14):
    recipe_website = f'https://www.mybakingaddiction.com/page/{q}/?s=brownies'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if item == 'recipes':
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# simplyrecipes Links
recipe_website = 'https://www.simplyrecipes.com/?s=brownies'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)

for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipes':
                    recipe_links.append(link)
                    print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# steamykitchen Links
for q in range(1,14):
    recipe_website = f'https://www.mybakingaddiction.com/page/{q}/?s=brownies'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if item == 'recipe':
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# tastesoflizzyt Links
for q in range(1,5):
    recipe_website = f'https://www.tastesoflizzyt.com/page/{q}/?s=brownies'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if 'brownies' in item:
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# tastykitchen Links
for q in range(1,38):
    recipe_website = f'https://tastykitchen.com/recipes/page/{q}/?s=brownies&view=rating&submit=k#038;submit=k'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if item == 'recipe':
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# thepioneerwoman Links
recipe_website = 'https://thepioneerwoman.com/?s=brownies'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)

for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if 'brownies' in item:
                    recipe_links.append(link)
                    print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# thepioneerwoman Links
recipe_website = 'https://thehappyfoodie.co.uk/search?keyword=brownies&filter=recipes&page=1'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)

for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipe':
                    recipe_links.append(link)
                    print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# tastykitchen Links
for q in range(1,38):
    recipe_website = f'https://tastykitchen.com/recipes/page/{q}/?s=brownies&view=rating&submit=k#038;submit=k'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if item == 'recipe':
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# twopeasandtheirpod Links
for q in range(1,3):
    recipe_website = f'https://www.twopeasandtheirpod.com/page/{q}/?s=brownies'
    driver = webdriver.Firefox()
    driver.get(recipe_website)
    print('!!!!!!!!!!!!!!!')
    print(q)

    for a in driver.find_elements_by_xpath('.//a'):
        links.append(a.get_attribute('href'))

        for link in links:
            if link is not None:
                for item in link.split('/'):
                    if 'brownie' in item:
                        recipe_links.append(link)
                        print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# whatsgabycooking Links

recipe_website = 'https://whatsgabycooking.com/?s=brownies'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)

for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))
    for link in links:
        if link is not None:
            for item in link.split('/'):
                if 'brownie' in item:
                    recipe_links.append(link)
                    print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

#Reset lists
links = []
recipe_links = []

#####
# yummly Links

recipe_website = 'https://www.yummly.com/recipes?q=brownies&gs=gf0nkx&showRegistrationModal=true&startEmailLogin=true'
driver = webdriver.Firefox()
driver.get(recipe_website)
print('!!!!!!!!!!!!!!!')
print(q)

for a in driver.find_elements_by_xpath('.//a'):
    links.append(a.get_attribute('href'))

    for link in links:
        if link is not None:
            for item in link.split('/'):
                if item == 'recipe':
                    recipe_links.append(link)
                    print(link)

for element in recipe_links:
    if element not in unique_recipe_links:
        unique_recipe_links.append(element)

####
# File Output
np.savetxt("unique_recipe_links.csv", unique_recipe_links, delimiter=",", fmt='%s', header=header)
