import numpy as np

with open('links.txt') as f:
    content = f.readlines()

unique_links = []

for element in content:
    if element not in unique_links:
        unique_links.append(element)

np.savetxt("recipe_links.csv", unique_links, delimiter=",", fmt='%s')
